//
//  UsersNetworkingMock.swift
//  CodingTestTests
//
//  Created by Adrian Zdanowicz on 01/10/2017.
//  Copyright © 2017 Adrian Zdanowicz. All rights reserved.
//

@testable import CodingTest

import Foundation
import RxSwift
import SwiftyJSON

class UsersNetworkingMock: UsersNetworking {
    
    func getUsers() -> Observable<[User]> {
        return Observable.create { observer in
            let usersList = Variable<[User]>([])
            if let users = UsersSerializer().unserialize(json: UsersResponseMock().getUsersJsonResponse()) {
                usersList.value = users
            }
            observer.onNext(usersList.value)
            return Disposables.create()
        }
    }
    
}

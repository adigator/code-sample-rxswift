//
//  UsersSerializerSpec.swift
//  CodingTestTests
//
//  Created by Adrian Zdanowicz on 01/10/2017.
//  Copyright © 2017 Adrian Zdanowicz. All rights reserved.
//

@testable import CodingTest

import Foundation
import Quick
import Nimble
import SwiftyJSON

fileprivate struct Keys {
    static let id = "id"
    static let login = "login"
    static let avatarUrl = "avatar_url"
}

class UsersSerializerSpec: QuickSpec {
    
    var usersSerializer: UsersSerializer!
    
    override func spec() {
        beforeEach {
            self.usersSerializer = UsersSerializer()
        }
        
        describe("UsersSerializer base funcionatlity") {
            
            it("should convert JSON data into User objects") {
                let response = UsersResponseMock().getUsersJsonResponse()
                var users = [User]()
                if let usersData = self.usersSerializer.unserialize(json: response) {
                    users = usersData
                }
                expect(users.count).to(equal(UsersResponseMock().objectsCount()))
                for (index, element) in users.enumerated() {
                    expect(element.id).to(equal(response[index][Keys.id].int))
                    expect(element.login).to(equal(response[index][Keys.login].string))
                    expect(element.avatarUrl).to(equal(response[index][Keys.avatarUrl].string))
                }
            }
        }
    }
    
}

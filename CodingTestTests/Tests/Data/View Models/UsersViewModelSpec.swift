//
//  UsersViewModelSpec.swift
//  CodingTestTests
//
//  Created by Adrian Zdanowicz on 01/10/2017.
//  Copyright © 2017 Adrian Zdanowicz. All rights reserved.
//

@testable import CodingTest

import Foundation
import Quick
import Nimble

class UsersViewModelSpec: QuickSpec {
    
    var usersViewModel: UsersViewModel!
    var networking: UsersNetworking!
    
    override func spec() {
        beforeEach {
            self.networking = UsersNetworkingMock()
            self.usersViewModel = UsersViewModelImpl(networking: self.networking)
        }
        
        describe("UsersViewModel base funcionatlity") {
            
            it("should load data") {
                expect(self.usersViewModel.getUsers().value.count).to(equal(0))
                self.usersViewModel.loadData()
                expect(self.usersViewModel.getUsers().value.count).to(equal(UsersResponseMock().objectsCount()))
            }
            
        }
    }
    
}

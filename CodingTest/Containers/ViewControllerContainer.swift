//
//  ViewControllerContainer.swift
//  CodingTest
//
//  Created by Adrian Zdanowicz on 01/10/2017.
//  Copyright © 2017 Adrian Zdanowicz. All rights reserved.
//

import Foundation

class ViewControllerContainer {
    
    fileprivate let viewModelContainer = ViewModelContainer()
    
    func getUsersViewController() -> UsersViewController {
        return UsersViewController(viewModel: viewModelContainer.getUsersViewModel())
    }
    
}

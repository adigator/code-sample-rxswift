//
//  ViewModelContainer.swift
//  CodingTest
//
//  Created by Adrian Zdanowicz on 01/10/2017.
//  Copyright © 2017 Adrian Zdanowicz. All rights reserved.
//

import Foundation

class ViewModelContainer {
    
    func getUsersViewModel() -> UsersViewModel {
        let serializer = UsersSerializer()
        let networking = UsersNetworkingImpl(serializer: serializer)
        return UsersViewModelImpl(networking: networking)
    }
    
}

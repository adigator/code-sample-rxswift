//
//  BasicTableView.swift
//  CodingTest
//
//  Created by Adrian Zdanowicz on 01/10/2017.
//  Copyright © 2017 Adrian Zdanowicz. All rights reserved.
//

import Foundation
import UIKit

class BasicTableView: UITableView {
    
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        rowHeight = UITableViewAutomaticDimension
        estimatedRowHeight = 120
        showsVerticalScrollIndicator = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

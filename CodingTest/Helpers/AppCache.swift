//
//  AppCache.swift
//  CodingTest
//
//  Created by Adrian Zdanowicz on 01/10/2017.
//  Copyright © 2017 Adrian Zdanowicz. All rights reserved.
//

import Foundation

class AppCache {
    
    private init() {}
    
    static let cache = NSCache<NSString, AnyObject>()
    
}

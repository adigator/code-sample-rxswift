//
//  UIImageView+urlImage.swift
//  CodingTest
//
//  Created by Adrian Zdanowicz on 01/10/2017.
//  Copyright © 2017 Adrian Zdanowicz. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

fileprivate struct UsersImage {
    static let placeholder = "userPlaceholder"
}

extension UIImageView {
    
    func setCachedImageFromUrl(imageUrl: String, completion: @escaping (() -> ())) {
        let cache = AppCache.cache
        if let img = getImage(imageUrl: imageUrl, completion: completion) {
            image = img
            cache.setObject(img, forKey: imageUrl as NSString)
        }
    }
    
    fileprivate func getImage(imageUrl: String, completion: @escaping (() -> ())) -> UIImage? {
        let cache = AppCache.cache
        if let image =  cache.object(forKey: imageUrl as NSString) as? UIImage  {
            return image
        } else {
            sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: UsersImage.placeholder), options: SDWebImageOptions.highPriority, completed: { (image, error, _, _) in
                if let image = image {
                    cache.setObject(image, forKey: imageUrl as NSString)
                }
                completion()
            })
            return nil
        }
    }
    
}

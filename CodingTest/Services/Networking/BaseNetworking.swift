//
//  BaseNetworking.swift
//  CodingTest
//
//  Created by Adrian Zdanowicz on 01/10/2017.
//  Copyright © 2017 Adrian Zdanowicz. All rights reserved.
//

import Foundation
import RxAlamofire
import RxSwift
import Alamofire

struct Endpoints {
    static let baseUrl = "https://api.github.com/"
    static let users = "users"
}

class BaseNetworking {
    
    func makeRequest(request: Request) -> Observable<(HTTPURLResponse, Any)> {
        return RxAlamofire.requestJSON(request.method, request.url, parameters: request.parameters, encoding: JSONEncoding.default, headers: request.headers).asObservable()
    }
    
}


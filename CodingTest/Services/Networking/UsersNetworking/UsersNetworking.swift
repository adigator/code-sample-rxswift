//
//  UsersNetworking.swift
//  CodingTest
//
//  Created by Adrian Zdanowicz on 01/10/2017.
//  Copyright © 2017 Adrian Zdanowicz. All rights reserved.
//

import Foundation
import RxSwift
import SwiftyJSON

enum UsersError: String, Error {
    case incorrectJSONObject
    case incorrectJSONArrayObject
}

protocol UsersNetworking {
    func getUsers() -> Observable<[User]>
}

class UsersNetworkingImpl: BaseNetworking, UsersNetworking {
    
    fileprivate let disposeBag = DisposeBag()
    fileprivate var serializer: UsersSerializer
    
    init(serializer: UsersSerializer) {
        self.serializer = serializer
    }
    
    func getUsers() -> Observable<[User]> {
        return Observable.create { observer in
            let request = Request(url: self.getUrl(), method: .get, parameters: nil, headers: nil)
            let _ = self.makeRequest(request: request).subscribe(onNext: {
                (response, json) in
                let jsonArray = JSON(json)
                if let users = self.serializer.unserialize(json: jsonArray) {
                    observer.onNext(users)
                } else {
                    observer.onError(UsersError.incorrectJSONArrayObject)
                }
            }, onError: {
                error in
                observer.onError(error)
            })
            return Disposables.create()
        }
    }
    
    fileprivate func getUrl() -> String {
        return Endpoints.baseUrl + Endpoints.users
    }
    
}

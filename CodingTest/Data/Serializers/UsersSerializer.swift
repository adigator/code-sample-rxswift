//
//  UsersSerializer.swift
//  CodingTest
//
//  Created by Adrian Zdanowicz on 01/10/2017.
//  Copyright © 2017 Adrian Zdanowicz. All rights reserved.
//

import Foundation
import SwiftyJSON

fileprivate struct Keys {
    static let id = "id"
    static let login = "login"
    static let avatarUrl = "avatar_url"
}

class UsersSerializer {
    
    func unserialize(json: JSON) -> [User]? {
        if let jsonArray = json.array {
            var users: [User] = []
            for jsonElement in jsonArray {
                if let element = unserializeSingleUser(json: jsonElement) {
                    users.append(element)
                }
            }
            return users
        } else {
            return nil
        }
    }
    
    fileprivate func unserializeSingleUser(json: JSON) -> User? {
        guard let id = json[Keys.id].int,
            let login = json[Keys.login].string,
            let avatarUrl = json[Keys.avatarUrl].string else {
                return nil
        }
        return User(id: id, login: login, avatarUrl: avatarUrl)
    }
    
}

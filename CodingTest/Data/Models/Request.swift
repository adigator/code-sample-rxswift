//
//  Request.swift
//  CodingTest
//
//  Created by Adrian Zdanowicz on 01/10/2017.
//  Copyright © 2017 Adrian Zdanowicz. All rights reserved.
//

import Foundation
import Alamofire

struct Request {
    
    var url: URLConvertible
    var method: HTTPMethod
    var parameters: Parameters?
    var headers: HTTPHeaders?
    
    init(url: URLConvertible,
         method: HTTPMethod,
         parameters: Parameters?,
         headers: HTTPHeaders?) {
        
        self.url = url
        self.method = method
        self.parameters = parameters
        self.headers = headers
    }
    
}

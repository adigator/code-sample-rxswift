//
//  User.swift
//  CodingTest
//
//  Created by Adrian Zdanowicz on 01/10/2017.
//  Copyright © 2017 Adrian Zdanowicz. All rights reserved.
//

import Foundation

struct User {
    
    var id: Int
    var login: String
    var avatarUrl: String
    
    init(id: Int, login: String, avatarUrl: String) {
        self.id = id
        self.login = login
        self.avatarUrl = avatarUrl
    }
    
}

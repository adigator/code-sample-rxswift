//
//  UserCell.swift
//  CodingTest
//
//  Created by Adrian Zdanowicz on 01/10/2017.
//  Copyright © 2017 Adrian Zdanowicz. All rights reserved.
//

import Foundation
import UIKit

class UserCell: UITableViewCell {
    
    fileprivate let mainView: UserCellView
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        mainView = UserCellView()
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = UIColor.white
        contentView.addSubview(mainView)
        mainView.setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUser(user: User) {
        mainView.login.text = user.login
        mainView.avatar.setCachedImageFromUrl(imageUrl: user.avatarUrl, completion: {})
    }
    
}

//
//  UserCellView.swift
//  CodingTest
//
//  Created by Adrian Zdanowicz on 01/10/2017.
//  Copyright © 2017 Adrian Zdanowicz. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

fileprivate struct Constants {
    struct Constraints {
        struct Avatar {
            static let top = 20
            static let horizontal = 25
        }
        struct Login {
            static let top = 20
            static let horizontal = 20
            static let bottom = 25
        }
    }
}

class UserCellView: UIView {
    
    let avatar: UIImageView = {
        let avatar = UIImageView()
        return avatar
    }()
    
    let login: UILabel = {
        let login = UILabel()
        login.textAlignment = .center
        return login
    }()
    
    func setupView() {
        snp.makeConstraints { (make) in
            make.top.bottom.trailing.leading.equalTo(0)
        }
        setupAvatar()
        setupLogin()
    }
    
    fileprivate func setupAvatar() {
        addSubview(avatar)
        avatar.snp.makeConstraints {
            make in
            make.top.equalTo(0).offset(Constants.Constraints.Avatar.top)
            make.leading.equalTo(0).offset(Constants.Constraints.Avatar.horizontal)
            make.trailing.equalTo(0).offset(-Constants.Constraints.Avatar.horizontal)
            make.height.equalTo(avatar.snp.width)
        }
    }
    
    fileprivate func setupLogin() {
        addSubview(login)
        login.snp.makeConstraints {
            make in
            make.top.equalTo(avatar.snp.bottom).offset(Constants.Constraints.Login.top)
            make.trailing.equalTo(-Constants.Constraints.Login.horizontal)
            make.leading.equalTo(Constants.Constraints.Login.horizontal)
            make.bottom.equalTo(0).offset(-Constants.Constraints.Login.bottom)
        }
    }
    
}

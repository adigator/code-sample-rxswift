//
//  UsersViewController.swift
//  CodingTest
//
//  Created by Adrian Zdanowicz on 01/10/2017.
//  Copyright © 2017 Adrian Zdanowicz. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxDataSources

fileprivate struct Constants {
    static let usersCellReuseIdentifier = "Users"
}

class UsersViewController: UIViewController {
    
    fileprivate var viewModel: UsersViewModel
    fileprivate let tableView: BasicTableView
    private let bag = DisposeBag()
    
    init(viewModel: UsersViewModel) {
        self.viewModel = viewModel
        self.tableView = BasicTableView()
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view = tableView
        setupTableView()
        viewModel.loadData()
    }
    
    fileprivate func setupTableView() {
        tableView.register(UserCell.self, forCellReuseIdentifier: Constants.usersCellReuseIdentifier)
        setupTableViewDataSource()
    }
    
    fileprivate func setupTableViewDataSource() {
        viewModel.getUsers().asObservable()
            .bind(to: tableView.rx.items(cellIdentifier: Constants.usersCellReuseIdentifier, cellType: UserCell.self)) { (_, element, cell) in
                cell.setUser(user: element)
            }
            .addDisposableTo(bag)
    }
    
}

//
//  UsersViewModel.swift
//  CodingTest
//
//  Created by Adrian Zdanowicz on 01/10/2017.
//  Copyright © 2017 Adrian Zdanowicz. All rights reserved.
//

import Foundation
import RxSwift

typealias UsersSuccess = (_ data: [User]) -> ()
typealias UsersFailure = (_ error: String?) -> ()

protocol UsersViewModel: class {
    func loadData()
    func getUsers() -> Variable<[User]>
}

class UsersViewModelImpl: UsersViewModel {
    
    private let bag = DisposeBag()
    private let usersList = Variable<[User]>([])
    private let networking: UsersNetworking
    
    init(networking: UsersNetworking) {
        self.networking = networking
    }
    
    func loadData() {
        networking
            .getUsers()
            .subscribe(onNext: {
                users in
                self.usersList.value = users
            }, onError: {
                error in
                print(error)
            })
            .addDisposableTo(bag)
    }
    
    func getUsers() -> Variable<[User]> {
        return usersList
    }
    
}
